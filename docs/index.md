# 🏡 Accueil

## Outils numériques


!!! info "Les bons logiciels"

    - **Libres** : pour de nombreuses raisons éthiques.
    - **Multiplateformes** : pour une utilisation avec Linux, Mac ou Windows, voire Android.
    - **Efficaces, maintenus, modernes et largement utilisés** : pour un gage de stabilité dans le temps.

    Quelques bons outils seront présentés par catégories.
    

!!! quote "Le partage du savoir"
    « Le savoir, n'est-ce pas, est un bien précieux.  
    Trop précieux pour ne pas être partagé ! »  
    *Battologio d'Épanalepse*  
    ![savoir_partage](./images/savoir_partage.jpg)  
    **De Cape et de Crocs** (Ayroles & Masbou), *tome vii, 14, 5*


## À propos de l'auteur

- Professeur agrégé de mathématiques, enseignant de NSI
- Formateur académique, en particulier pour le langage Python
- Niveau 5 sur France-IOI, uniquement avec Python
- [Créateur de problèmes d'algorithmique](https://www.spoj.com/problems/FRANCKY/) _(en)_
    - Membre du bureau éditorial sur SPOJ
