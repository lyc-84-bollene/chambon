# Programmation

## Les commentaires

!!! tip "Bonnes pratiques pour les commentaires de code"
    Une [liste créée par la communauté de _Stack Overflow_](https://stackoverflow.blog/2021/12/23/best-practices-for-writing-code-comments/) (2021/12/23)

    - Règle 1 : Les commentaires ne doivent pas répéter le code.

    - Règle 2 : De bons commentaires n'excusent pas un code obscur.

    - Règle 3 : Si vous ne pouvez pas écrire un commentaire clair, alors il y a peut-être un problème avec le code.

    - Règle 4 : Les commentaires devraient dissiper toute trace de confusion, pas en créer.

    - Règle 5 : On explique le code non conventionnel en commentaires.

    - Règle 6 : On donne le lien vers l'origine d'un code source copié.

    - Règle 7 : On inclut des liens vers des références externes **là où** elles sont le plus utiles.

    - Règle 8* : On ajoute un doctest quand on règle un bogue.

    - Règle 9* : On utilise `#!python raise NotImplementedError` pour indiquer une implémentation incomplète.

    > Règles 8 et 9 modifiées pour l'utilisation de Python, par votre serviteur.

!!! example "Exemples de mauvais commentaires"

    ```python
    i = 0         # un entier
    
    i = i + 1     # ajoute un à i
    
    # répéter 10 fois
    for _ in range(10):
        x = 0 if bin(i)[-1] == '0' else 1  # le dernier caractère de l'écriture en binaire de i transformé en entier
    ```

Comme l'écrivent Kernighan et Plauger dans _The Elements of Programming Style_,
> « On ne commente pas de mauvais code, on le réécrit. »


## Efficacité

- Oui, le temps humain est plus précieux que le temps machine.
- Oui, l'optimisation à outrance et la surqualité sont à chasser en production classique.
- Mais, ce n'est pas une raison pour créer des monstres applicatifs.

!!! cite "Le désenchantement du logiciel"
    [À lire et méditer.](https://blog.romainfallet.fr/desenchantement-logiciel/)

    - Tout est insupportablement lent
    - Tout est ÉNORME
    - Tout finit par devenir obsolète
    - L'apologie de la médiocrité
    - Programmer, c'est le même bordel
    - On est bloqué avec
    - Les entreprises ne s'y intéressent pas
    - Tout n'est pas mauvais
    - Manifeste pour un monde meilleur

