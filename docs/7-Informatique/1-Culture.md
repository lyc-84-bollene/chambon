# Culture geek

## XKCD

![compiling](images/compiling.png)

[XKC](https://xkcd.com/303/) est un _webcomic_ (sous licence libre) qu'il est parfois délicat de comprendre.

## Pepper & Carrot

![peppercarrot](images/peppercarrot.png)

[Un webcomic libre et open-source](https://www.peppercarrot.com/) financé directement par ses lecteurs pour changer l'industrie de la BD !


