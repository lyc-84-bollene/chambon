# Éléments d'histoire de l'informatique


!!! info "Bibliographie"
    ![](assets/Wikipedia-logo-v2.svg){ align=right width="200" }
    Ce document ne fait que reprendre différents passages de Wikipédia. On évoque :
    
    - des figures historiques de l'informatique,
    - différents systèmes d'exploitation,
    - certains de leurs auteurs.

## Avant les ordinateurs

### Ada Lovelace

![](assets/Ada_lovelace.jpg){ align=right width="200" }

[Ada Lovelace](https://fr.wikipedia.org/wiki/Ada_Lovelace), comtesse de Lovelace, le 10 décembre 1815 à Londres et morte le 27 novembre 1852 à Marylebone dans la même ville, est une pionnière de la science informatique.

Ada est la fille de Lord Byron, célèbre poète anglais, et Annabella Milbanke, une femme intelligente et cultivée. Annabella adorait les mathématiques. Byron l'appelait même parfois « la princesse des parallélogrammes ». Annabella fit en sorte que les tuteurs d'Ada lui donnent une éducation approfondie en mathématiques et en sciences, ce qui était tout à fait inhabituel à l'époque dans l'éducation d'une jeune fille de la noblesse. 

Elle est principalement connue pour avoir réalisé le premier véritable programme informatique, lors de son travail sur un ancêtre de l'ordinateur : la machine analytique de Charles Babbage. Dans ses notes, on trouve en effet le premier programme publié, destiné à être exécuté par une machine, ce qui fait considérer Ada Lovelace comme « le premier programmeur du monde ». Elle a également entrevu et décrit certaines possibilités offertes par les calculateurs universels, allant bien au-delà du calcul numérique et de ce qu'imaginaient Babbage et ses contemporains.

### Alan Turing

![](assets/Alan_Turing_Aged_16.jpg){ align=right width="200" }

[Alan Turing](https://fr.wikipedia.org/wiki/Alan_Turing), né le 23 juin 1912 à Londres et mort le 7 juin 1954 à Wilmslow, est un mathématicien et cryptologue britannique, auteur de travaux qui fondent scientifiquement l'informatique. 

Pour résoudre le problème fondamental de la décidabilité en arithmétiques, il présente en 1936 une expérience de pensée que l'on nommera ensuite machine de Turing et des concepts de programme et de programmation, qui prendront tout leur sens avec la diffusion des ordinateurs, dans la seconde moitié du XXe siècle.

Durant la Seconde Guerre mondiale, il joue un rôle majeur dans la cryptanalyse de la machine Enigma utilisée par les armées allemandes : l'invention de machines usant de procédés électroniques, les bombes, fera passer le décryptage à plusieurs milliers de messages par jour. Ce travail secret ne sera connu du public que dans les années 1970. Après la guerre, il travaille sur un des tout premiers ordinateurs, puis contribue au débat sur la possibilité de l'intelligence artificielle, en proposant le test de Turing. Vers la fin de sa vie, il s'intéresse à des modèles de morphogenèse du vivant conduisant aux « structures de Turing ». 

## Multics

[Multics](https://fr.wikipedia.org/wiki/Multics) fut conçu à partir de 1964, conjointement par le MIT, les Laboratoires Bell et General Electric.

Quand les Laboratoires Bell se retirèrent du projet, deux ingénieurs qui travaillaient sur Multics (Ken Thompson et Dennis Ritchie) lancèrent leur propre projet baptisé initialement UNICS (_UNiplexed Information and Computing Service_) par opposition entre _Uniplexed/Multiplexed_ du projet Multics qu'ils jugeaient beaucoup trop compliqué. Le nom fut ensuite modifié en UNIX.

!!! info "première GUI"
    ![](assets/me9xobqwmna01.jpg){ width= "300" }

    Première démonstration publique d'une souris d'ordinateur, interface graphique pour utilisateur (GUI), fenêtres et traitement de texte. 1968.

## UNIX

En 1969, Ken Thompson qui travaillait alors pour les laboratoires Bell développa la première version d'un système d'exploitation mono-utilisateur sous le nom de New Ken's System. Le nom Unics fut suggéré par Brian Kernighan à la suite d'un jeu de mots « latin » avec Multics ; « Multi- car Multics faisait la même chose de plusieurs façons alors qu'Unics faisait chaque chose d'une seule façon ». Ce nom fut par la suite contracté en Unix (pour être déposé finalement sous le nom [UNIX](https://fr.wikipedia.org/wiki/Unix) par AT&T), à l'initiative de Brian Kernighan.

## Langage C

En 1971, conscient de la difficulté que représente la maintenance d'un système écrit en langage d'assemblage, Ken Thompson songea à réécrire Unix en TMG, mais il trouva que le TMG n'offrait pas ce dont il avait besoin. Pendant une courte période il songea à réécrire Unix en Fortran, mais finalement conçut le B avec l'aide de Dennis Ritchie dans les années 1969 et 1970, en s'inspirant du langage BCPL. Cependant Unix ne fut jamais réécrit en B ; le B ne supportait pas les types, toutes les variables étaient de la même taille que les mots de l'architecture, l'arithmétique sur les flottants n'était pas implémentée ; de plus, le compilateur B utilisait la technique du _threaded code_. C'est pourquoi Dennis Ritchie entreprit en 1971 d'écrire le New B, qui fut renommé en C. Le langage C est toujours l'un des langages les plus utilisés aujourd'hui.

```C
#include <stdio.h>
int main() {
   // printf() affiche la chaine de caractère entre guillemets
   printf("Hello, World!");
   return 0;
}
```

## L'expansion d'UNIX

![](assets/Histoire_d'UNIX.svg)

L'incompatibilité grandissante entre les nombreuses variantes d'Unix proposées par les différents éditeurs pour les différentes machines porte peu à peu atteinte à la popularité et à la diversité des systèmes Unix. De nos jours, les systèmes Unix propriétaires, longtemps majoritaires dans l'industrie et l'éducation, sont de moins en moins utilisés. En revanche, trois systèmes de type Unix basés sur BSD (FreeBSD, NetBSD et OpenBSD) d'une part, et le système GNU/Linux, compatible Unix, d'autre part, ainsi que macOS (anciennement OS X, basé sur Darwin), occupent une part de marché de plus en plus importante, permettant à Unix de concurrencer l'autre grande famille d'OS (propriétaire), Windows NT.

## GNU

[GNU](https://fr.wikipedia.org/wiki/GNU) est un système d'exploitation lancé en 1983 par Richard Stallman dans le but de fournir un équivalent d'Unix composé uniquement de logiciel libre.

En 1991, alors que le noyau de GNU, le Hurd trainait à être opérationnel, fut créé le noyau Linux (voir ci-dessous) qui sortit en 1992. Cela permit d'utiliser pour la première fois un système d'exploitation entièrement libre, une variante de GNU utilisant le noyau Linux connue sous le nom de GNU/Linux, ou plus couramment, simplement Linux.

GNU et GNU/Linux sont utilisés sous la forme de distributions qui les accompagnent de logiciels supplémentaires. Parmi les distributions les plus populaires, on compte notamment Debian, Ubuntu, Linux Mint, Red Hat, Fedora et Arch.

![](assets/distros/Debian-OpenLogo.svg){ width="100" }
![](assets/distros/Logo-ubuntu_cof-orange-hex.svg){ width="100" }
![](assets/distros/Linux_Mint_logo_without_wordmark.svg){ width="100" }
![](assets/distros/Red_Fedora.svg){ width="100" }
![](assets/distros/Fedora_icon_(2021).svg){ width="100" }
![](assets/distros/Archlinux-icon-crystal-64.svg){ width="100" }

## Linux

En 1991 un étudiant finlandais, Linus Torvalds, décida de concevoir, sur le modèle de Minix, un système d'exploitation capable de fonctionner sur les architectures à base de processeur Intel 80386. Le noyau, qui était alors au stade expérimental, devait être généré sur un système Minix.

Le nom de Linux vient de la personne qui hébergeait le projet pour sa diffusion (version 0.0.1) et non d'un choix de Linus. Il voulut un temps rebaptiser son système Freax, mais il était trop tard, Linux s'était déjà imposé auprès des aficionados. [Linux](https://fr.wikipedia.org/wiki/Linux) ne contient aucun code provenant de UNIX, il en est juste inspiré, et complètement réécrit. D'autre part, Linux est un logiciel libre.

Depuis novembre 2017, Linux est le seul système d'exploitation utilisé par les 500 [supercalculateurs](https://fr.wikipedia.org/wiki/Superordinateur) les plus puissants du monde. Les autres systèmes Unix équipaient encore quelques-uns de ces ordinateurs en 2016.

![](assets/ordis/CDC_6600.jc.jpg){ width="100" }
![](assets/ordis/Cray2.jpeg){ width="100" }
![](assets/ordis/Columbia_Supercomputer_-_NASA_Advanced_Supercomputing_Facility.jpg){ width="100" }
![](assets/ordis/FSL_JET_Supercomputer.jpg){ width="100" }
![](assets/ordis/IBM_Blue_Gene_P_supercomputer.jpg){ width="100" }
![](assets/ordis/Mira_-_Blue_Gene_Q_at_Argonne_National_Laboratory.jpg){ width="100" }


## Android

Android est un système d'exploitation pour terminaux mobiles développé par une startup du même nom et racheté par Google. Il est basé sur le noyau Linux.

Plusieurs systèmes d'exploitation pour appareils mobiles (smartphones, tablettes, PDA…) sont des systèmes Unix. On peut citer en particulier iOS et Android, qui se partagent plus de 85 % du marché des smartphones.

## Personnages clés

### Ken Thompson

![](assets/Ken_Thompson_02.jpg){ align=right width="200" }

[Ken Thompson](https://fr.wikipedia.org/wiki/Ken_Thompson) (né le 4 février 1943 à La Nouvelle-Orléans) est un informaticien américain, concepteur des systèmes Unix et Plan 9 ainsi que des langages B et Go.
Dans les années 1970, il crée Belle avec Joseph Condon, un ordinateur spécialisé dans le jeu d'échecs. Belle remporte à plusieurs reprises le championnat nord-américain des ordinateurs d'échecs entre 1978 et 1986.

Il participe à de nombreuses évolutions du système Unix : portage sur PDP-11, réécriture en langage C, et introduction des pipes. 

Il est également l'inventeur de l'éditeur ed et avec Rob Pike du codage des caractères UTF-8. 

Il a pris sa retraite des Bell Labs en 2000. Il a travaillé à Entrisphere comme conseiller scientifique jusqu'en 2006 et travaille désormais chez Google. 

### Dennis Ritchie

![](assets/Dennis_Ritchie_2011.jpg){ align=right width="200" }

[Dennis MacAlistair Ritchie](https://fr.wikipedia.org/wiki/Dennis_Ritchie), né le 9 septembre 1941 à Bronxville dans l'État de New York, est un des pionniers de l'informatique moderne, inventeur du langage C et codéveloppeur de Unix.

Au début des années 1970, programmeur aux Laboratoires Bell, il travaille avec Ken Thompson au développement d'Unix. Le langage B de Thompson étant trop limité pour les besoins du nouveau système, Ritchie est amené à créer sur les mêmes bases le langage C. Par la suite, avec l'aide de Brian Kernighan, il promeut le langage et rédige notamment le livre de référence *The C Programming Language*.

Il reçoit conjointement avec Ken Thompson le prix Turing de l'ACM en 1983 pour leur travail sur le système Unix.

![](assets/The_C_Programming_Language,_First_Edition_Cover.svg){ width="300" }

### Richard Stallman

![](assets/Richard_Stallman_at_LibrePlanet_2019.jpg){ align=right width="200" }

[Richard Stallman](https://fr.wikipedia.org/wiki/Richard_Stallman), né le 16 mars 1953 à Manhattan, est un programmeur et militant du logiciel libre. Initiateur du mouvement du logiciel libre, il lance, en 1983, le projet GNU et la licence publique générale GNU connue aussi sous le sigle GPL. Il a popularisé le terme anglais « copyleft ». Programmeur renommé de la communauté informatique américaine et internationale, il a développé de nombreux logiciels dont les plus connus des développeurs sont l'éditeur de texte GNU Emacs, le compilateur C de GNU (GCC), le débogueur GNU (gdb) mais aussi, en collaboration avec Roland McGrath, le moteur de production GNU Make.

![](assets/Heckert_GNU_white.svg){ width="300" }

### Linus Torvalds

![](assets/LinuxCon_Europe_Linus_Torvalds_03_(cropped).jpg){ align=right width="200" }

Linus Torvalds, né le 28 décembre 1969 à Helsinki en Finlande, est un informaticien américano-finlandais.

Il est connu pour avoir créé en 1991 (à 21 ans) le noyau Linux dont il continue de diriger le développement. Il est considéré comme le « dictateur bienveillant à vie » (*Benevolent Dictator for Life*).

Il a également créé le logiciel de gestion de versions décentralisée Git.

![](assets/Tux.svg){ width="300"}


## Version humoristique

[_A Brief, Incomplete, and Mostly Wrong History of Programming Languages_](http://james-iry.blogspot.com/2009/05/brief-incomplete-and-mostly-wrong.html)

Par exemple :

> _1986 - Brad Cox and Tom Love create Objective-C, announcing "this language has all the memory safety of C combined with all the blazing speed of Smalltalk." Modern historians suspect the two were dyslexic._

> _1987 - Larry Wall falls asleep and hits Larry Wall's forehead on the keyboard. Upon waking Larry Wall decides that the string of characters on Larry Wall's monitor isn't random but an example program in a programming language that God wants His prophet, Larry Wall, to design. Perl is born._

>_1996 - James Gosling invents Java. Java is a relatively verbose, garbage collected, class based, statically typed, single dispatch, object oriented language with single implementation inheritance and multiple interface inheritance. Sun loudly heralds Java's novelty._

>_2001 - Anders Hejlsberg invents C#. C# is a relatively verbose, garbage collected, class based, statically typed, single dispatch, object oriented language with single implementation inheritance and multiple interface inheritance. Microsoft loudly heralds C#'s novelty._
