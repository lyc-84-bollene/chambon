# Créations informatiques

(La FORGE est désormais opérationnelle)[https://fchambon.forge.apps.education.fr/classe/8-productions/]

Présentation de quelques travaux personnels ou en équipe. Ces travaux ont vocation à être repris et transférés sur la [FORGE des Communs Numériques de l'Éducation](https://forge.apps.education.fr/), si ce n'est pas déjà fait.

## 2024, Structures arborescentes

[Structures arborescentes](https://pratique.forge.apps.education.fr/arbre/){ .md-button }

- cours complet, détaillé, avec de nombreux compléments,
- avec près d'une centaine d'exercices de difficultés très variées,
- permet aussi aux meilleurs élèves de s'exercer sur des problèmes difficiles dans le cadre du programme, en autonomie grâce à un IDE intégré.
- Extraits du contenu (sans IDE) :

=== "Arbre binaire, cours"

    !!! abstract "[Définitions](https://pratique.forge.apps.education.fr/arbre/1-arbre-bin/10-def/)"

        !!! quote "Définition"
            Un **arbre binaire** est un ensemble fini, **éventuellement vide**, de nœuds structurés de manière connexe, acyclique, hiérarchique et ordonnée.
            
            Chaque nœud possède **exactement deux sous-arbres**, un à gauche, un à droite.

        !!! note "Définition récursive"
            - Un arbre binaire peut avoir zéro nœud, il est alors vide, on le note _nil_. C'est un arbre binaire vide.
            - Sinon, un arbre binaire non vide possède un nœud particulier, sa racine, et deux sous-arbres indépendants, un à gauche et un à droite, qui sont des arbres binaires.

        !!! example "Dessin"
            Un arbre binaire peut se dessiner avec les _nil_, ou non, mais il faut bien indiquer la direction des sous-arbres.
            
            Chaque nœud possède **exactement deux sous-arbres**, un à gauche, un à droite. Un arbre (comme un sous-arbre) peut être vide.

            ```mermaid
            graph TB
                N0("11")
                N0 --> N1("42")
                N0 --> N2("11")
                N1 --> N11(" ")
                N1 --> N12("21")
                N12 --> N121(" ")
                N12 --> N122(" ")
                N2 --> N21(" ")
                N2 --> N22(" ")
            ```

            Ci-dessus un arbre binaire ayant 4 nœuds et de hauteur 3.

        !!! quote "Définition"
            La **taille** d'un arbre binaire est son nombre de nœuds.

        !!! note "Propriété récursive"
            - La taille d'un arbre binaire vide vaut zéro.
            - Sinon, l'arbre possède une racine et deux sous-arbres ; sa taille vaut **1** (pour la racine) **plus** la taille de son sous-arbre à gauche **plus** la taille de son sous-arbre à droite.

=== "classe `Noeud`"

    !!! info "[Modélisation avec une classe `Noeud`](https://pratique.forge.apps.education.fr/arbre/2-exo-ab/00-rappel/)"
        Dans tous les exercices de cette section notée (🌱), un arbre binaire sera donné :

        - par `#!py None` si c'est un arbre binaire vide,
        - sinon (il est non vide), par son nœud racine. Un nœud sera une instance de la classe suivante :

        ```python
        class Noeud:
            def __init__(self, gauche, label, droite):
                self.gauche = gauche
                self.label = label
                self.droite = droite
        ```

        - `gauche` désignera son sous-arbre à gauche, ce sera donc ou bien `#!py None` **ou bien** une instance de `Noeud`.
        - `label` pourra être de tout type (cela dépendra de l'exercice), c'est l'étiquette portée par la racine.
        - `droite` désignera son sous-arbre à droite, ce sera donc ou bien `#!py None` **ou bien** une instance de `Noeud`.
    
    !!! danger "Changements pour 2024/2025"
        La classe `Noeud` sera très probablement réécrite de manière préfixe :
        
        ```python
        class Noeud:
            def __init__(self, label, gauche=None, droite=None):
                self.label = label
                self.gauche = gauche
                self.droite = droite
        ```


=== "Arbre binaire, exercice"

    !!! abstract "[Un arbre binaire est-il dégénéré ?](https://pratique.forge.apps.education.fr/arbre/2-exo-ab/20-est_degenere/)"

        !!! quote "Définition"
            Un arbre binaire est dit **dégénéré** si sa hauteur est égale à sa taille.

        !!! example "Exemple"
            Voici un arbre binaire de hauteur 4 et de taille 4 ; il est dégénéré !

            ```mermaid
            graph TB
                N0("42")
                N0 --> N00("13")
                N0 --> N01(" ")
                N00 --> N000(" ")
                N00 --> N001("5")
                N001 --> N0000(" ")
                N001 --> N0001("8")
                N0001 --> A(" ")
                N0001 --> B(" ")
            ```

        !!! question "Exercice"
            Coder la fonction `est_dégénéré` qui prend `ab` un arbre binaire en paramètre représenté à l'aide de la classe `Noeud` et qui renvoie un booléen :

            - `#!py True` si `ab` est dégénéré,
            - `#!py False` sinon.


        !!! warning "Sans utiliser directement la formule de la définition"
            On demande de coder une fonction qui est indépendante des fonctions `hauteur` et `taille`.

            On peut faire sans parcourir tout l'arbre ; par exemple si la racine possède deux sous-arbres non vides, la réponse est non-dégénéré sans avoir à parcourir le reste de l'arbre.

            ??? guide "Guide"

                ```python
                def est_dégénéré(ab):
                    "Renvoie un booléen, True si ab est dégénéré, False sinon"
                    if ab is None:
                        return ...           # Aide 1. (1)
                    elif ab.gauche is None:
                        return est_dégénéré(...)  # 4. (4)
                    elif ...:                     # 3. (3)
                        return ...                # 5. (5)
                    else:
                        # Les deux sous-arbres sont non vides
                        return ...                # 2. (2)
                ```

                1. Un booléen, cas simple ; regardez les tests
                2. Cas simple aussi ; lire le commentaire
                3. Par symétrie du cas précédent
                4. Le seul appel récursif raisonnable
                5. Par symétrie



=== "ABR, exercice"

    !!! abstract "[Rotations d'arbre binaire de recherche](https://pratique.forge.apps.education.fr/arbre/4-exo-abr/52-rotations/)"

        !!! info "Rotations"
            - Les deux ABR suivants possèdent les mêmes valeurs, mais n'ont pas la même structure.
            - Les trois formes trapézoïdales représentent un schéma d'ABR (éventuellement vide).
            - On passe de l'un à l'autre par une rotation sur la racine !

            !!! example "Avec racine 10"

                ```mermaid
                graph TB
                    R(10)
                    R --> S[/"Valeurs\n x ≤ 10"\]
                    R --> A("20")
                    A --> Ag/[/"Valeurs\n 10 < x ≤ 20"\]
                    A --> Ad[/"Valeurs\n 20 < x"\]
                ```

                Avec une rotation **à gauche** on obtient l'ABR suivant :

            !!! example "Avec une rotation à gauche effectuée"

                ```mermaid
                graph TB
                    R(20)
                    R --> A("10")
                    A --> S[/"Valeurs\n x ≤ 10"\]
                    A --> Ag/[/"Valeurs\n 10 < x ≤ 20"\]
                    R --> Ad[/"Valeurs\n 20 < x"\]
                ```

                Avec une rotation **à droite** on obtient l'ABR précédent :



        !!! question "Exercice"
            Coder la fonction `rotation_gauche` qui prend en paramètre un ABR non vide, dont le sous-arbre à droite est non vide et qui **modifie** l'ABR suivant cette transformation.
            
            De même, coder la fonction `rotation_droite` qui prend en paramètre un ABR non vide, dont le sous-arbre à gauche est non vide et qui **modifie** l'ABR suivant cette transformation.

            - :+1: la classe ABR est utilisée ; elle est disponible dans l'IDE.
            - :+1: On garantit que les tests proposent des ABR valides, prêts pour la rotation, ainsi, ils sont non vides et il y a aussi un sous-arbre non vide du bon côté.



        ??? tip "Indice"
            - C'est assez simple en suivant le schéma !
            - On pourra modifier le nœud racine directement.
            - Mais, il **faudra** toutefois créer un ABR vide avec le créateur `ABR()` pour le nouveau nœud qui va accueillir ensuite les autres morceaux.


            ??? guide "Guide"

                ```python
                def rotation_gauche(abr):
                    tmp = abr.label
                    petits = ...
                    moyens = abr.droite.gauche
                    grands = ...

                    abr.label = ...
                    ... = ABR()
                    abr.gauche.label = ...

                    abr. ... = petits
                    abr.gauche.droite = ...
                    abr. ... = ...

                def rotation_gauche(abr):
                    ...  # un peu de symétrie
                ```

=== "Arbre enraciné, exercice"
    !!! abstract "[Nombre de Strahler](https://pratique.forge.apps.education.fr/arbre/8-exo-arbre/54-Strahler/)"

        😀 On considère ici des arbres **non étiquetés**.

        !!! quote "Définition du nombre de Strahler d'un arbre"
            Le nombre de Strahler d'un arbre indique sa complexité de branchement ; il est défini pour chacun des nœuds.
            
            Le nombre de Strahler d'un arbre est celui de sa racine.

            - Le nombre de Strahler d'une feuille est 1.
            - Pour tout autre nœud, on note `i` le maximum des nombres de Strahler de ses enfants (il y en a).
                - S'il y a **un seul enfant** dont le nombre de Strahler est `i`, alors le nombre de Strahler du nœud reste égal à `i`,
                - sinon, le nombre de Strahler du nœud est égal à `i + 1`.

        ??? info "Culture scientifique"
            Le paramètre « nombre de Strahler » fut introduit pour la première fois par Horton et Strahler en Hydrogéologie, pour l'étude de la _forme_ des bassins fluviaux. Depuis il est réapparu dans l'étude des formes des structures arborescentes naturelles : arbres en botanique, éclairs en physique, molécules biologiques d'ARN, etc.

        !!! example "Exemple d'arbre ayant un nombre de Strahler égal à 2"

            <div class="grid" markdown>

            ```mermaid
            graph TB
                R{ } --> N1{ }
                R    --> N3{ }
                N3   --> N4{ }
                N3   --> N5{ }
                N3   --> N6{ }
                N4   --> N7{ }
            ```

            ```mermaid
            graph TB
                R(2) --> N1(1)
                R    --> N3(2)
                N3   --> N4(1)
                N3   --> N5(1)
                N3   --> N6(1)
                N4   --> N7(1)
            ```

            </div>


        !!! question "Exercice"
            Coder la fonction `nb_strahler` qui prend un `arbre` non étiqueté en paramètre et qui renvoie un entier : le nombre de Strahler de sa racine.

        ??? tip "Indice"
            Cet exercice s'apparente à une recherche de maximum, pour lequel on vérifie s'il est unique ou non. Ceci, de manière récursive.

            ??? guide "Guide"

                ```python
                def nb_strahler(arbre):
                    "Renvoie le nombre de Strahler de l'arbre"
                    enfants = arbre
                    if ...:
                        return 1
                    else:
                        n_max = ...
                        for sous_arbre in enfants:
                            n = ...
                            if n == n_max:
                                seul = False  # le maximum n'est pas unique
                            elif n > n_max:
                                n_max = ...
                                seul = True
                            else:
                                ...

                        if ...:
                            n_max += 1
                        return n_max
                ```
=== "Autres jolis exercices"
    - [Arbre quaternaire pour image](https://pratique.forge.apps.education.fr/arbre/2-exo-ab/90-quad/){ .md-button }
    - [Reconstruction d'arbres binaires de recherche](https://pratique.forge.apps.education.fr/arbre/4-exo-abr/53-reconstruction/)
    - [Arbre miroir](https://pratique.forge.apps.education.fr/arbre/8-exo-arbre/40-miroir/)
    - [Plus petit ancêtre commun](https://pratique.forge.apps.education.fr/arbre/8-exo-arbre/65-ancetre_commun/)

## Depuis 2022, en équipe

[Site vitrine du groupe e-nsi](https://e-nsi.forge.aeif.fr/){ .md-button }

Ce travail collectif réalisé depuis 2022 par le groupe e-nsi, propose de la création de contenu et la validation entre pairs. Une très forte émulation a été générée.

Nous avons pu proposer, entre autres :

- des exercices en ligne avec autocorrection, (Python, SQL),
- des cours variés, avec des compléments,
- la traduction de [FutureCoder](https://fr.futurecoder.io/) en français,
- des exercices d'écrit corrigés, d'après des sujets de BAC.

Strictement aucune donnée personnelle ne transite, aucune installation n'est nécessaire, le site est utilisable sur téléphone, tablette ou PC. Utilisation exclusive de logiciels libres, le contenu est lui-aussi sous licence libre. Suite au départ de M. Bouillot hors de l'enseignement, nous avons la chance d'avoir Frédéric Zinelli qui a réécrit presque entièrement l'intégration de Pyodide. Notre travail prend alors une nouvelle forme depuis 2024 :

[CodEx](https://codex.forge.apps.education.fr/){ .md-button }

!!! info "Reconnaissances"
    Ce travail a aussi permis à de nombreux enseignants de suivre nos échanges constructifs, à la fois sur l'espace public du forum, mais aussi dans une section dédiée pleine d'émulation. Nous avons reçu de nombreux messages d'encouragements.

!!! info "Membres principaux"

    - Franck Chambon, auteur principal du contenu, relectures, validations, traductions
    - Nicolas Revéret, auteur important du contenu, relectures, validations, traductions
    - Romain Janvier, relectures, validations
    - Vincent Bouillot, première intégration de Pyodide dans MkDocs
    - une ou deux dizaines de participants plus ou moins occasionnels

    Désormais Frédéric Zinelli s'occupe de l'intégration de Pyodide dans MkDocs.


!!! examples "Exemples personnels dans CodEx"

    - [Anniversaire de chat](https://codex.forge.apps.education.fr/exercices/channiv/)
    - [Soleil couchant](https://codex.forge.apps.education.fr/exercices/soleil_couchant/)
    - [Formes pleines et formes creuses](https://codex.forge.apps.education.fr/exercices/formes/)
    - [Suite audioactive de Conway](https://codex.forge.apps.education.fr/exercices/conway/)
    - [Suite de Hofstadter-Conway à 10000$](https://codex.forge.apps.education.fr/exercices/HC10000/)
    -  Pavage possible avec triominos [V1](https://codex.forge.apps.education.fr/exercices/pavage_triomino/) [V2](https://codex.forge.apps.education.fr/exercices/pavage_triomino_2/)
    - [Sortie de labyrinthe](https://codex.forge.apps.education.fr/exercices/labyrinthe/)
    - [Nombre serpent](https://codex.forge.apps.education.fr/exercices/nb_serpent/)



## 2020 - 2022


### Informatique au lycée

[Cours sur la récursivité](https://e-nsi.forge.aeif.fr/recursif/){ .md-button } avec de [nombreuses fractales](https://e-nsi.forge.aeif.fr/recursif/4-Fractal/5-fractales_2/3-cercles_3/).

[Cours sur les structures linéaires](https://franck.forge.aeif.fr/struct-lineaires/){ .md-button }

[Corrections commentées de code d'élèves sur Prologin](https://e-nsi.forge.aeif.fr/prologin/2003/E01/) ; idéal après un devoir maison

[Initiation à Python, aidé par France IOI](https://e-nsi.forge.aeif.fr/init_python/) ; idéal SNT ou maths en seconde

Toutes ces créations ont vocations à rejoindre la FORGE des Communs du Numérique de l'Éducation après mise à jour.

### Mathématiques au lycée

Pour des élèves de seconde, quelques questions autour de Python

- [Exercices flash](https://ens-fr.gitlab.io/flash/){ .md-button } : travail initialement commencé avec Alexandre Colin alors que nous étions formateur Python dans l'académie d'Aix-Marseille.

Pour des élèves de Spécialité Maths en première

- [Compléments en première](https://ens-fr.gitlab.io/math1/){ .md-button } : quelques chapitres de cours avec des exercices
- [Calcul des chiffres de pi](https://fchambon.forge.aeif.fr/piday/){ .md-button } : un complément de cours sur l'histoire et le calcul de $\pi$.



## 2020-2022 : Travail avec MkDocs

Ayant donné une conférence en mars 2020 pour la journée des mathématiques sur le thème du dénombrement et des liens possibles avec Python, j'ai ensuite rassemblé mes carnets sur un site statique écrit avec MkDocs

[Énumération](https://ens-fr.gitlab.io/enumeration/){ .md-button } : un mélange de mathématiques et d'algorithmique avancée.

Cette présentation a eu du succès, **sur sa forme**, auprès des enseignants de NSI, j'ai alors écrit un tutoriel à destination à la fois des élèves et des collègues enseignants demandeurs : <https://ens-fr.gitlab.io/mkdocs/>. Nous sommes, depuis, nombreux à utiliser cette technologie de construction de sites web statiques qui allie la simplicité de Markdown et les possibilités de script de Python. Les travaux de mes élèves de première [2021](https://ens-fr.gitlab.io/tuto-markdown/1-Creations_eleves/1-art_fractal/) et [2022](https://ens-fr.gitlab.io/eleves-md/) montrent également le potentiel didactique **et** pédagogique. 

!!! example "Quatre exemples de productions d'élèves remarquables"

    - [L'art fractal](https://ens-fr.gitlab.io/tuto-markdown/1-Creations_eleves/1-art_fractal/) ; une élève de terminale qui, avec NSI + APla a intégré une école d'architecture
    - [L'animation](https://ens-fr.gitlab.io/eleves-md/1/A/Animation/) ; une élève de première qui, avec NSI + APla + (maths complémentaire) a intégré l'INSA Lyon et se régale désormais à faire de la mécatronique
    - [Langages de programmation](https://okonore.github.io/languages_programmation/) ; un élève alors en première qui, ensuite avec NSI + Maths, a intégré le CMI à Avignon et réussit brillamment.
    - [La langue finnoise](https://ens-fr.gitlab.io/eleves-md/T/A/intro/) ; un élève de terminale ayant fait un voyage scolaire en pays nordique et qui poursuit de belles études de langues et civilisation, son travail en NSI lui a beaucoup apporté.


Travail personnel réalisé essentiellement en 2020 et 2021, qui ne couvre qu'une partie du programme

- SNT : <https://ens-fr.gitlab.io/algo0/>
- NSI-1 : <https://ens-fr.gitlab.io/algo1/>
- NSI-2 : <https://ens-fr.gitlab.io/algo2/>

:warning: Ces sites se retrouvent fréquemment cités chez certains collègues, mais j'avoue ne plus assumer toutes les positions prises. D'où la réécriture récente de certains pans.


Pour les collègues, une présentation d'outils numériques : <https://fchambon.forge.apps.education.fr/classe/>



## 2016-2020 : Des carnets Jupyter

[J'utilisais aussi souvent les carnets Jupyter](https://github.com/FranckCHAMBON/Python-Lycee), autant pour mes recherches personnelles que pour des vacations à l'Université d'Avignon. Alors formateur académique en Python avec le groupe E-Mathice, j'ai recommandé l'utilisation de Capytale au sein de l'académie d'Aix-Marseille.

L'essentiel de mes carnets porte sur l'algorithmique et les mathématiques discrètes.

## 2012 - 2016 : Résolutions et créations de problèmes

[Créateur de problèmes d'algorithmique de compétition](https://www.spoj.com/problems/FRANCKY/) ; SPOJ (en anglais) est la plus grande banque mondiale, j'en suis devenu membre du bureau éditorial après avoir résolu des exercices phares, en proposé de nouveaux et participé activement à aider des utilisateurs. Ces problèmes concernent majoritairement les mathématiques : algèbre et théorie des nombres, réservés aux étudiants avec une solide formation. Certains ont servi pour des phases finales de la [Bubble Cup](https://bubblecup.org/) organisée par Microsoft.

:warning: avec [Project Euler](https://projecteuler.net/), c'est là qu'on trouve les exercices les plus difficiles. La particularité de SPOJ est de proposer la résolution dans le langage de son choix et d'établir un classement par rapidité d'exécution de code, ce qui peut pousser à la recherche d'optimisation, parfois très formatrice, parfois anti pédagogique.


## Avant 2012 : LaTeX

!!! info "LaTeX"
    :+1: LaTeX permet de produire des documents de haute qualité typographique.

    :warning: il est très difficile de faire du travail collaboratif dans le secondaire avec LaTeX ; excluant, car trop technique et chronophage.

=== "Cours"

    !!! note "Premier cours sur Python"

        ![Initiation à l'algorithmique](./initPython3.pdf){ type=application/pdf style="width:40em;height:37em" }

=== "Exercices"

    !!! question "50 traductions/résolutions de problèmes sur ProjectEuler"

        ![Problèmes du Projet Euler](./ProjectEuler.pdf){ type=application/pdf style="width:40em;height:37em" }
