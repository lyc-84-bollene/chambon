# Différentes utilisations

!!! question "Aucune expérience ?"
    Commencer par suivre un [cours](https://ens-fr.gitlab.io/algo1/) d'algorithmique avec Python. Vous n'avez rien à installer.

Vous connaissez déjà un peu Python, et vous souhaitez l'utiliser pour progresser. C'est possible avec une tablette, un ordinateur. Avec ou sans installation. On commence sans installation.

## Utilisation en ligne

Connecté à Internet, on peut débuter Python sans installer de logiciel.

Depuis 2021, il existe une solution qui respecte bien le RGPD : [Basthon](https://basthon.fr/).

Depuis son navigateur web, on se rend sur la [console](https://console.basthon.fr/).

Possible avec Android, Linux, Mac ou Windows.

!!! abstract "Exemple"
    On peut travailler avec les coordonnés GPS sur la carte _OpenStreetMap_.
    <iframe src="https://console.basthon.fr/?script=eJzLzC3ILypRSMvPySzN5eXi5cpVsIXy9HwTCzRy8pMTSzLz82yjTUz0jCxNTHUUTPTMTWN1eLkUUEFVfn5ufHFJYlGJraG5JtgovZTM4oKcxEoNTQAbiRyF" width=100% height=700 onload="window.scrollTo({ top: 0, behavior: 'smooth' });"></iframe>

## Utilisation hors ligne

!!! warning "Android"

    Il existe quelques applications, mais elles sont :

    - souvent non libres, avec des annonces publicitaires,
    - difficiles à utiliser sans clavier physique.

    Il existe au moins une bonne solution :

    1. Une possibilité efficace repose sur Termux avec l'éditeur micro. **Nous l'aborderons plus tard.**

    :+1: Si possible, en utilisant un clavier physique.

!!! tip "Thonny pour débuter"
    La solution la plus simple pour commencer est d'utiliser [Thonny](../2-Éditeurs/1-thonny.md).

    Thonny est un logiciel qui :

    - est libre, simple et robuste ;
    - est disponible pour Linux, Mac et Windows ;
    - possède sa version de Python intégrée.
    - ne travaille **qu'avec** le langage Python.

!!! success "VSCodium pour progresser"
    Après la phase d'apprentissage du premier éditeur, on peut envisager d'utiliser un éditeur polyvalent. Vous pourrez l'utiliser pour **tous** les langages de programmation.

    Parmi les possibilités, il y a :

    - Vim ou Emacs, qui sont utilisés depuis très longtemps. Ils sont délicats à utiliser et manquent de modernité.
    - Geany est léger, simple et robuste, c'est aussi un très bon choix.
    - VSCodium est moderne et puissant. C'est un excellent choix. Il faudra juste faire attention à ne pas utiliser VSCode qui est la version distribuée par MicroSoft et qui contient de la télémétrie, ainsi que dans certaines de ses extensions. **Ne pas utiliser VSCode.**

    Cependant, ces éditeurs multi langages ne possèdent pas Python intégré, ni un autre langage... Il faut avoir installé le moteur Python.

