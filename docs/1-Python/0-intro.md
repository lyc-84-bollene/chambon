# Sommaire

- Différentes utilisations
    - en ligne,
    - ou hors ligne
- Installation de Python
    - avec Linux
    - avec Mac
    - avec Windows
    - avec Android
- Ressources pédagogiques
    - de plusieurs niveaux
    - de plusieurs origines
