# Installation


## Installation de Python

=== "Linux"
    **Facile**. Python est déjà installé.
    
    Vous pouvez vérifier avec  `python` ou `python3`

    ```console
    utilisateur@machine:~$ python --version
    Python 3.X.Y
    ```

    Un numéro de version plus précis sera affiché à la place de `3.X.Y`

=== "Mac"
    **Moyen**. Python n'est peut-être pas installé.
    
    Vous pourriez vérifier

    ```console
    $ python --version
    Python 3.X.Y
    ```

    Si ce n'est pas le cas, [télécharger et installer Python3](
        https://www.python.org/downloads/mac-osx/) : _Latest Python 3 Release_


=== "Windows"
    **Attention**. Python n'est peut-être pas installé, ou mal installé. :warning:
    
    Vous pourriez vérifier

    ```console
    C:\> py --version
    Python 3.X.Y
    ```

    Si ce n'est pas le cas, [télécharger et installer Python3](
        https://www.python.org/downloads/windows/) : _Latest Python 3 Release_

    !!! warning "Intégrer Python dans le PATH"
        Lors de l'installation, il faut penser à cocher la case :

        ![python path pour windows](py_win.png){width=100%}

=== "Android"
    **Délicat**. On pourra utiliser [Termux](../5-Système/1-termux.md) et micro avec Python.

    Ce n'est pas le plus simple pour commencer.



## Installation de `pip`

Pour installer des modules supplémentaires avec Python, la solution la plus simple est d'utiliser l'**i**nstallateur de **p**aquet **p**ython (pip).


=== "Linux"
    Vérification

    ```console
    utilisateur@machine:~$ python3 -m pip --version
    pip X.Y.Z from .../site-packages/pip (python X.Y)
    ```
    
    !!! abstract "Installation de pip"
        
        Pour l'administrateur de la machine, la solution la plus simple est d'entrer (pour une version Linux de type Debian) :

        ```console
        utilisateur@machine:~$ sudo apt install python3-pip
        ```

        Sinon, [suivre ce lien...](
        https://packaging.python.org/guides/installing-using-linux-tools/)


    Vous pouvez alors vérifier votre version de pip.

    ```console
    utilisateur@machine:~$ python3 -m pip --version
    pip X.Y.Z from .../site-packages/pip (python X.Y)
    ```

    Vous pourrez alors, à tout moment, mettre à jour pip avec

    ```console
    utilisateur@machine:~$ python3 -m pip install -U pip
    ```

=== "Mac"
    Vérification

    ```console
    $ python -m pip --version
    pip X.Y.Z from .../site-packages/pip (python X.Y)
    ```

    Si besoin, l'administrateur de la machine doit installer `pip`

     - Il télécharge le script `get-pip.py` sur bootstrap,
     - il le lance avec Python :

    ```console
    $ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    $ python get-pip.py
    ```

    Vous pouvez alors vérifier votre version de pip.

    ```console
    $ python -m pip --version
    pip X.Y.Z from .../site-packages/pip (python X.Y)
    ```

    Vous pourrez alors, à tout moment, mettre à jour pip avec

    ```console
    $ python -m pip install -U pip
    ```

=== "Windows"
    Si besoin, l'administrateur de la machine doit installer `pip`

     - Il télécharge le script `get-pip.py` sur bootstrap,
     - il le lance avec Python :

    ```console
    C:\> curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    C:\> py get-pip.py
    ```

    Vous pouvez alors vérifier votre version de pip.

    ```console
    C:\> py -m pip --version
    pip X.Y.Z from ...\site-packages\pip (python X.Y)
    ```

    Vous pourrez alors, à tout moment, mettre à jour pip avec

    ```console
    C:\> py -m pip install -U pip
    ```

=== "Android"
    Termux se comporte Linux, regarder donc la section adaptée.
