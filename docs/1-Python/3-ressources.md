# 📚 Ressources sur Python

Pour une présentation encyclopédique : :fontawesome-brands-wikipedia-w: [Python](https://fr.wikipedia.org/wiki/Python_%28langage%29)

On présente ici une liste en lien avec Python au lycée.

## La documentation

!!! cite "Officielle"
    1. :fontawesome-brands-python: Python offre une [documentation très complète, en français](https://docs.python.org/fr/3/) pour une immense partie.
        - 🔖 À enregistrer dans les marque-pages.
        - 🔍 À utiliser pour faire des [recherches](https://docs.python.org/fr/3/search.html?q=&check_keywords=yes&area=default)
    2. Le [tutoriel officiel](https://docs.python.org/fr/3/tutorial/index.html)
        - 📅 Parties 1 à 5 conseillées, après cette présentation-ci.
    3. Dans la bibliothèque standard
        - 🔢 Lire sur les [types numériques](https://docs.python.org/fr/3/library/stdtypes.html#numeric-types-int-float-complex)
    4. La [référence du langage](https://docs.python.org/fr/3/reference/index.html)
        - ⚡ À lire pour devenir un pro.

!!! example "Des cours"
    - Le livre [Apprendre à programmer avec Python3](https://inforef.be/swi/download/apprendre_python3_5.pdf) de Gérard Swinnen.
        - Chapitres 1 à 7, puis 10.
    - Le livre [Une introduction à Python 3](https://perso.limsi.fr/pointal/_media/python:cours:courspython3.pdf) de Bob Cordeau & Laurent Pointal.
        - Chapitres 1 à 5.
    - Sur [Zeste de savoir](https://zestedesavoir.com/),
        - [les bases](https://zestedesavoir.com/tutoriels/799/apprendre-a-programmer-avec-python-3/) (3h30) puis
        - [parties avancées](https://zestedesavoir.com/tutoriels/954/notions-de-python-avancees/) (12h).
    - Sur [OpenClassRoom](https://openclassrooms.com/fr/)
        - la [partie 1](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python) du cours sur Python.
    - Sur [developpez.com](https://www.developpez.com/),
        - [un](https://flossmanuals.developpez.com/tutoriels/debuter/initiation-python/)
        parmi [d'autres](https://python.developpez.com/cours/).

!!! info "Journal"
    Voici [une série d'articles](https://linuxfr.org/news/python-pour-la-fin-de-l-annee-2021) sur [linuxfr.org](https://linuxfr.org) pour les plus curieux au sujet de Python.

## Ressources d'activités

### Pour les élèves

1. [FranceIOI](http://www.france-ioi.org/algo/chapters.php)
    - 👍 Propose un suivi de la classe.
    - 👍 Une grande quantité d'activités.
    - ⚠️ Un peu vieillot ; peu attractif.
    - ⚠️ Utilise trop `input` et `print`.
2. [Prologin](https://prologin.org/)
    - 👍 Entièrement en français ; problèmes de qualité.
    - ⚠️ Difficile pour les élèves. À réserver aux meilleurs.
2. [Capytale](https://capytale2.ac-paris.fr/web/c-auth/list)
    - 👍 Grande banque d'activités.
3. [Questions Flash](https://ens-fr.gitlab.io/flash/)
    - 👍 Exercices rapides pour le lycée.
    - 👍 Fait avec MkDocs.
4. [Pyvert](https://diraison.github.io/Pyvert/)
    - 👍 Exercices auto-corrigés.
    - 👍 Plusieurs niveaux d'exercices au lycée.
    - 👍 Fait avec MkDocs.
5. [Carnets.info](https://www.carnets.info/)
    - 👍 Site d'une collègue de NSI, également formatrice Python.
    - 👍 Avec de jolis exercices.
    - 👍 Fait avec MkDocs.
6. [Algorithmique et Mathématiques avec Python](https://ens-fr.gitlab.io/algo1/)
    - 😎 Site de votre serviteur.
    - 👍 Contient un cours moderne et de nombreux exercices
    - 👍 Fait avec MkDocs.

### Autres sites

Des sites qui nécessitent une inscription. :warning: Cela ne respecte pas le RGPD. :warning:
On donne donc la liste uniquement à but informatif.
Ils sont toutefois de bonne qualité.


1. [Project Euler](https://projecteuler.net/)
    - 👍 Progressif et devient très vite une belle source de problèmes mathématiques.
    - ⚠️ En anglais, et vite difficile.
2. [CodinGame](https://www.codingame.com/)
    - 👍 Très attractif visuellement.
    - ⚠️ Pour les meilleurs élèves de NSI.
3. [SPOJ](https://www.spoj.com/problems/classical/sort=6)
    - 👍 Énorme banque d'exercices.
    - 👍 [Section débutant](https://www.spoj.com/problems/basics/sort=6)
    - ⚠️ En anglais, et utilise trop `input` et `print`.
4. [codewars](https://www.codewars.com/?language=python)
    - 👍 Travaille avec des fonctions, et moins avec `input` et `print`.
    - ⚠️ En anglais, niveau vite difficile pour des NSI.
5. [HackerRank - Python](https://www.hackerrank.com/domains/python), [HackerRank - Maths](https://www.hackerrank.com/domains/mathematics)
    - 👍 Bonne source d'idées d'activités.
    - ⚠️ En anglais, et utilise trop `input` et `print`.
6. [LeetCode](https://leetcode.com/)
    - 👍 Très bonne source d'idées d'activités.
    - ⚠️ En anglais, et plutôt pour les enseignants.
7. [W3Schools](https://www.w3schools.com/python/exercise.asp?filename=exercise_syntax1)
    - 👍 Interactif, facile et progressif.
    - ⚠️ En anglais. Uniquement pour les bases ; rien en algorithmique.

### _Serious games_

Pour jouer et apprendre à programmer ; des jeux intelligents :

- [RoboZZle](http://www.robozzle.com/beta/index.html)
- [CargoBot](http://www-verimag.imag.fr/~wack/CargoBot/)
- [Blockly Games](https://blockly.games/?lang=fr)
