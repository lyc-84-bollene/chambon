# Micro Editor

Un éditeur de code minimaliste, mais très puissant, utilisable en console uniquement.

Disponible pour Android, Linux, Mac et Windows.

<https://micro-editor.github.io/>

À venir
