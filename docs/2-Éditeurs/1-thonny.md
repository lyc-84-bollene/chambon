# Thonny

C'est l'éditeur de code Python que l'on recommande à utiliser en premier sur ordinateur !

- Très simple.
- Multiplateforme, libre, robuste, assez complet.


## Installation de Thonny

=== "Linux"
    L'administrateur entre

    ```bash
    sudo apt-get install thonny
    ```

=== "Mac"
    L'administrateur installe la dernière version disponible, sur

    [le site officiel](https://thonny.org/){ .md-button }

=== "Windows"
    L'administrateur installe la dernière version disponible, sur

    [le site officiel](https://thonny.org/){ .md-button }

## Utilisation

Quand on travaille sur un fichier Python, l'extension doit être en `.py`

Vous observez la coloration syntaxique du code.

:+1: Quand on appuie sur ++f5++, le code s'exécute.


!!! example "Exemple"

    ```python
    print("Hello World!")

    import sys
    print(sys.version)
    ```

    Essayez d'exécuter ce code, votre premier code !
    