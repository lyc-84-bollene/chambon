# Choisir un éditeur

![clavier](assets/xkeyboarcd.png)

## Au sujet de l'Azerty

[Histoire de l'Azerty](https://ploum.net/le-bepo-nest-pas-un-numero-cest-un-clavier-libre/)

## Choisir une police pour coder

Faire [le test](https://sourcefoundry.org/hack/playground.html)

- Cliquer sur _compare side-by-side_
- Choisir _Size_ à 20 pour bien voir
- Choisir le langage Python
- Choisir deux polices à comparer
- Insister sur **Fira Mono** que l'on recommandera aux débutants
    - **Fira Code** sera idéale pour un codeur expérimenté qui n'a pas peur des ligatures
- Vérifier que **Courier** est un très mauvais choix.
- Utiliser le code ci-dessous pour tester les caractères proches graphiquement

```python
for texte_confus in [
    "44 A4 4A AA",
    "99 g9 9g gg",
    "11 l1 1l ll",
    "00 O0 0O OO",
    "zz 2z z2 22",
    "55 S5 5S SS",
    "66 G6 6G 66",
    "88 B8 8B BB",
    "77 17 71 11",
    "yy vy yv vv",
]:
    if len(texte_confus) >= 0:
        print(texte_confus)
```

!!! tip "Les très bonnes polices pour coder"

    - Fira Mono
    - Inconsolata
    - DejaVu Sans Mono
    - Fira Code

!!! info "Autour de l ou L pour Litre"
    Lire [l'article](https://fr.wikipedia.org/wiki/Claude_Litre).
    > Claude (Émile Jean-Baptiste) Litre est une personnalité scientifique fictive créée en 1978 par Ken Woolner de l'université de Waterloo dans le but de justifier l'utilisation d'un « L » en majuscule pour symboliser le litre, en lieu et place d'un « l » en minuscule comme le prévoient normalement les règles du Système international d'unités (SI).


