# Geany

Geany est un éditeur de code léger pour ordinateur, libre, multiplateforme, robuste et très utilisé.

Il reste simple, mais il n'est pas dédié uniquement à Python.
Il lui faut donc une version de Python3 déjà installée sur la machine.

Après Thonny, vous pouvez vous entrainer avec Geany et jouer aussi avec d'autres langages.

## Installation de Python3

=== "Linux"
    Vous avez déjà Python3.

    Conseil : Vous pouvez ajouter `alias python=python3` à la fin de votre fichier `~/.bashrc`.

=== "Mac"
    L'administrateur installe Python via le [site officiel](https://www.python.org/downloads/mac-osx/){ .md-button }

    :warning: Il est important de cocher la case « Inclure Python dans le PATH »

=== "Windows"
    L'administrateur installe Python via le [site officiel](https://www.python.org/downloads/windows/){.md-button}

    :warning: Il est important de cocher la case « Inclure Python dans le PATH »

## Installation de Geany

=== "Linux"
    L'administrateur entre

    ```bash
    sudo apt-get install geany
    ```

=== "Mac"
    L'administrateur installe la dernière version disponible, sur

    [le site officiel](https://geany.org/){ .md-button }

=== "Windows"
    L'administrateur installe la dernière version disponible, sur

    [le site officiel](https://geany.org/){ .md-button }

## Utilisation

Quand on travaille sur un fichier Python, l'extension doit être en `.py`

Vous observez la coloration syntaxique du code.

Quand on appuie sur ++f5++, le code s'exécute.
