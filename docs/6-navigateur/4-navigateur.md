# Navigateur

Des articles à lire pour comprendre

- [Les Cookies, qui sont-ils ? Que veulent-ils ?](https://www.laquadrature.net/2021/05/28/les-cookies-qui-sont-ils-que-veulent-ils/)
- [Firefox](https://www.mozilla.org/fr/firefox/new/), le navigateur qui protège ce qui est important.
- [Google risque 5 milliards de dollars pour avoir pisté les utilisateurs de Chrome en mode privé.](https://www.phonandroid.com/google-risque-5-milliards-de-dollars-pour-avoir-piste-les-utilisateurs-de-chrome-en-mode-prive.html)


Bonnes extensions avec Firefox, pour ceux qui n'aiment pas la publicité ni être pistés :

- [uBlock Origin, pour Firefox](https://addons.mozilla.org/fr/firefox/addon/ublock-origin/), un bloqueur de publicité.
- [Privacy Badger](https://addons.mozilla.org/fr/firefox/addon/privacy-badger17/), apprend automatiquement à bloquer les _trackers_ invisibles.
- [AdNauseam](https://adnauseam.io/), clique sur les publicités bloquées, et noie votre activité, cela évite de se faire pister.
- [Decentraleyes](https://decentraleyes.org/) évite le tracking via le contenu 'libre' centralisé.
- [Cookie AutoDelete](https://addons.mozilla.org/fr/firefox/addon/cookie-autodelete/) ; choisissez une liste blanche de **sites utiles** où vous conservez les _cookies_, et détruisez automatiquement les autres.
- Penser à la navigation privée, le plus souvent possible, sauf raison impérative.

